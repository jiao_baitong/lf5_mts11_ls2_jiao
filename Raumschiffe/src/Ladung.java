public class Ladung {

    private String name;
    private int anzahl;

    /** Default Konstruktor
     *
     */
    public Ladung() {

    }

    /** Vollparametrisierter Konstruktor
     *
     * @param name - Initialer Name als String
     * @param anzahl - Initiale Anzahl als int
     */
    public Ladung(String name, int anzahl) {
        setName(name);
        setAnzahl(anzahl);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAnzahl() {
        return anzahl;
    }

    public void setAnzahl(int anzahl) {
        this.anzahl = anzahl;
    }

    /** Gibt die Ladung als Bezeichnung und Anzahl als String aus
     *
     * @return Ein String mit Bezeichnung, Anzahl
     */
    @Override
    public String toString() {
        return name + anzahl;
    }
}
