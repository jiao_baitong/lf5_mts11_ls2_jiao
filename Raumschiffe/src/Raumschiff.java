import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/** Die Klasse Raumschiff kann Ladungen mit sich tragen und mit anderen Schiffen interagieren in der Form sie abzuschiessen.
 * Das Attribut broadcastKommunikator is statisch und wird somit von allen Raumschiffen geteilt.
 * Ein Raumschiff kann sich mithilfe von reparaturAndroiden reparieren.
 */
public class Raumschiff {

    private String name;
    private double energieversorgung;
    private double schutzschilde;
    private double lebenserhaltungssysteme;
    private double huelle;
    private int photonentorpedo;
    private int reparaturAndroiden;
    private List<Ladung> ladungsverzeichnis;
    private static ArrayList<String> broadcastKommunikator = new ArrayList<>();

    /** Default Konstruktor
     *
     */
    public Raumschiff() {

    }

    /** Vollparametrisierter Konstruktor
     *
     * @param name zu initialisierender Name als String
     * @param energieversorgung zu initialisierender Wert der Energieversorgung als double
     * @param schutzschilde zu initialisierender Wert der Schutzschilde als double
     * @param lebenserhaltungssysteme zu initialisierender Wert der Lebenserhaltungssysteme als double
     * @param huelle zu initialisierender Wert der Huelle als double
     * @param photonentorpedo zu initialisierende Anzahl der photonenTorpedos als int
     * @param reparaturAndroiden zu initialisierende Anzahl der reparaturAndroiden als int
     * @param ladungsverzeichnis zu initialisierende List der Ladungen als {@literal List<Ladung>}
     */
    public Raumschiff(String name, double energieversorgung, double schutzschilde,
                      double lebenserhaltungssysteme, double huelle, int photonentorpedo,
                      int reparaturAndroiden, List<Ladung> ladungsverzeichnis) {
        setName(name);
        setEnergieversorgung(energieversorgung);
        setSchutzschilde(schutzschilde);
        setLebenserhaltungssysteme(lebenserhaltungssysteme);
        setHuelle(huelle);
        setPhotonentorpedo(photonentorpedo);
        setReparaturAndroiden(reparaturAndroiden);
        setLadungsverzeichnis(ladungsverzeichnis);
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getEnergieversorgung() {
        return energieversorgung;
    }

    /** Die Energieversorgung darf ueber 100% sein, da im Objektdiagramm die Anforderung bestand, dass ein Schiff eine Energieversorgung von 120% hat.
     * Die Angabe des Attributs erfolgt als double, d.h. 100% = 1.0
     * @param energieversorgung der zu setzende Wert
     */
    public void setEnergieversorgung(double energieversorgung) {
        if (energieversorgung >= 0)
            this.energieversorgung = energieversorgung;
    }

    public double getSchutzschilde() {
        return schutzschilde;
    }

    /** Die Schutzschilde darf ueber 100% sein, da im Objektdiagramm die Anforderung bestand, dass ein Schiff eine Energieversorgung von 120% hat.
     * Es wurde gleiches Verhalten für Konsistenz gewählt.
     * Die Angabe des Attributs erfolgt als double, d.h. 100% = 1.0
     * @param schutzschilde der zu setzende Wert
     */
    public void setSchutzschilde(double schutzschilde) {
        if (schutzschilde >= 0)
            this.schutzschilde = schutzschilde;
    }

    public double getLebenserhaltungssysteme() {
        return lebenserhaltungssysteme;
    }

    /** Die Lebenserhaltungssysteme darf ueber 100% sein, da im Objektdiagramm die Anforderung bestand, dass ein Schiff eine Energieversorgung von 120% hat.
     * Es wurde gleiches Verhalten für Konsistenz gewählt.
     * Die Angabe des Attributs erfolgt als double, d.h. 100% = 1.0
     * @param lebenserhaltungssysteme der zu setzende Wert
     */
    public void setLebenserhaltungssysteme(double lebenserhaltungssysteme) {
        if (lebenserhaltungssysteme >= 0)
            this.lebenserhaltungssysteme = lebenserhaltungssysteme;
    }

    public double getHuelle() {
        return huelle;
    }

    /** Die Huelle darf ueber 100% sein, da im Objektdiagramm die Anforderung bestand, dass ein Schiff eine Energieversorgung von 120% hat.
     * Es wurde gleiches Verhalten für Konsistenz gewählt.
     * Die Angabe des Attributs erfolgt als double, d.h. 100% = 1.0
     * @param huelle der zu setzende Wert
     */
    public void setHuelle(double huelle) {
        if (huelle >= 0)
            this.huelle = huelle;
    }

    public int getPhotonentorpedo() {
        return photonentorpedo;
    }

    public void setPhotonentorpedo(int photonentorpedo) {
        if (photonentorpedo >= 0)
            this.photonentorpedo = photonentorpedo;
    }

    public int getReparaturAndroiden() {
        return reparaturAndroiden;
    }

    public void setReparaturAndroiden(int reparaturAndroiden) {
        if (reparaturAndroiden >= 0)
            this.reparaturAndroiden = reparaturAndroiden;
    }

    public List<Ladung> getLadungsverzeichnis() {
        return ladungsverzeichnis;
    }

    public void setLadungsverzeichnis(List<Ladung> ladungsverzeichnis) {
        this.ladungsverzeichnis = ladungsverzeichnis;
    }

    public static ArrayList<String> getBroadcastKommunikator() {
        return broadcastKommunikator;
    }

    public static void setBroadcastKommunikator(ArrayList<String> broadcastKommunikator) {
        Raumschiff.broadcastKommunikator = broadcastKommunikator;
    }

    /** Eine Ladung wird dem Ladungsverzeichnis des Raumschiffes hinzugefuegt.
     * @param neueLadung die hinzuzufuegende Ladung
     */
    public void addLadung(Ladung neueLadung) {
        ladungsverzeichnis.add(neueLadung);
    }

    /** Gibt den Zustand des Objektes in der Konsole aus.
     *
     */
    public void zustandAufKonsoleAusgeben() {
        System.out.println("Raumschiffname: " + name);
        System.out.println("Energieversorgung: " + energieversorgung * 100 + "%");
        System.out.println("Schutzschilde: " + schutzschilde * 100 + "%");
        System.out.println("Lebenserhaltungssysteme: " + lebenserhaltungssysteme * 100 + "%");
        System.out.println("Hülle: " + huelle * 100 + "%");
        System.out.println("Photonentorpedo: " + photonentorpedo);
        System.out.println("Reparatur-Androiden: " + reparaturAndroiden);
    }

    /** Gibt das Ladungsverzeichnis in der Konsole aus.
     *
     */
    public void ladungsverzeichnisAufKonsoleAusgeben() {
        System.out.println("Ladungsverzeichnis von " + name);
        System.out.println(ladungsVerzeichnisToString());
    }

    /** Voraussetzung: Falls weniger als 1 Torpedo vorhanden wird nachrichtAnAlleSenden()
     * aufgerufen. Ansonsten wird wirdGetroffen mit dem Ziel aufgerufen.
     *
     * @param ziel Das Zielraumschiff
     */
    public void photonentorpedosAbschiessen(Raumschiff ziel) {
        if (photonentorpedo < 1)
            nachrichtAnAlleSenden("-=*Click*=-");
        else {
            nachrichtAnAlleSenden("Photonentorpedo abgeschossen");
            wirdGetroffen(ziel);
        }
    }

    /** Voraussetzung: Falls Schild des Ziels weniger als 50%,
     * dann werden Schilde des Ziels 0 gesetzt, und wenn zusaetzlich
     * die Energieversorgung weniger als 50% betraegt,
     * wird die Energieversorgung 0 gesetzt, ansonten wird die Energieversorgung
     * um 50% reduziert. Falls die Huelle zusaetzlich weniger als 50% uebrig hat,
     * wird diese und die Lebenserhaltungssysteme 0 gesetzt. Ansonsten wird
     * die Huelle um 50% reduziert. Falls die Schutzschilde ueber 50% liegen werden
     * diese um 50% reduziert.
     *
     * @param ziel Das Zielraumschiff.
     */
    public void wirdGetroffen(Raumschiff ziel) {
        System.out.println(ziel.getName() + " wurde getroffen!");
        double zielSchild = ziel.getSchutzschilde();
        double zielHuelle = ziel.getHuelle();
        double zielEnergieversorgung = ziel.getEnergieversorgung();
        if (zielSchild < 0.5) {
            ziel.setSchutzschilde(0);
            if (zielEnergieversorgung < 0.5)
                ziel.setEnergieversorgung(0);
            else ziel.setEnergieversorgung(zielEnergieversorgung - 0.5);
            if (zielHuelle <= 0.5) {
                ziel.setHuelle(0);
                ziel.setLebenserhaltungssysteme(0);
                nachrichtAnAlleSenden("Lebenserhaltungssysteme von " + ziel.getName() + " sind zerstört.");
            }
            else ziel.setHuelle(zielHuelle - 0.5);
        }
        else ziel.setSchutzschilde(zielSchild - 0.5);
    }

    /** Voraussetzung: Falls die Energieversorgung unter 50% liegt wird nur
     * nachrichtAnAlleSenden() aufgerufen. Ansonsten wird wirdGetroffen() mit dem Ziel aufgerufen.
     * @param ziel Das Zielraumschiff
     */
    public void phaserkanonenAbschiessen(Raumschiff ziel) {
        if (energieversorgung < 0.5)
            nachrichtAnAlleSenden("-=*Click*=-");
        else {
            energieversorgung -= 0.5;
            nachrichtAnAlleSenden("Phaserkanone abgeschossen");
            wirdGetroffen(ziel);
        }
    }

    /** Nachricht wird dem boardcastKommunikator hinzugefuegt.
     *
     * @param nachricht Die zu sendende Nachricht
     */
    public static void nachrichtAnAlleSenden(String nachricht) {
        broadcastKommunikator.add(nachricht);
    }

    /** Gibt den broadcastKommunikator zurueck
     *
     * @return broadcastKommunikator
     */
    public static List<String> getLogbuchEintraege() {
        return broadcastKommunikator;
    }

    /** Voraussetzung: Falls keine Photonentorpedos im Ladungsverzeichnis sind,
     * wird nur nachrichtAnAlleSenden() aufgerufen. Ansonsten falls höchstens genausoviele
     * vorhanden sind wie aufgerufen wird die Anzahl der Ladung auf 0 gesetzt und die Photonentorpedos
     * im Schiff um die Anzahl hochgesetzt. Falls mehr vorhanden sind wird die Anzahl der Ladung um
     * die aufgerufene Anzahl heruntergesetzt.
     * @param anzahl Anzahl der Torpedos welche geladen werden sollen
     */
    public void photonenTorpedosLaden(int anzahl) {
        if (ladungsverzeichnis.stream().noneMatch(l -> "Photonentorpedo".equals(l.getName()))) {
            System.out.println("Keine Photonentorpedos gefunden!");
            nachrichtAnAlleSenden("-=*Click*=-");
        }
        else {
            int vorhanden;
            for (Ladung ladung : ladungsverzeichnis) {
                if ("Photonentorpedo".equals(ladung.getName())) {
                    vorhanden = ladung.getAnzahl();
                    if (anzahl >= vorhanden) {
                        photonentorpedo += anzahl;
                        ladung.setAnzahl(0);
                    }
                    else {
                        photonentorpedo += anzahl;
                        ladung.setAnzahl(vorhanden - anzahl);
                    }
                }
            }
            System.out.println("Photonentorpedo(s) eingesetzt");
        }
    }

    /** Entfernt Ladungen mit Anzahl 0 vom Ladungsverzeichnis.
     *
     */
    public void ladungsverzeichnisAufraeumen() {
        ladungsverzeichnis.removeIf(ladung -> ladung.getAnzahl() == 0);
    }

    /** Repariert die Systeme im Schiff, fuer welche die Parameter auf true gesestz sind.
     * Die Systeme werden um eine Zufallszahl zwischen 1 und 100 mal der Anzahl der Androiden geteilt
     * durch die Anzahl der zu reparierenden Schiffsstrukturen repariert.
     *
     * @param sollSchutzschildRepariertWerden Parameter fuer Schitzschilde
     * @param sollLebenserhaltungssystemRepariertWerden Parameter fuer Lebenserhaltungssysteme
     * @param sollHuelleRepariertWerden Parameter fuer Huelle
     * @param anzahlAndroiden Anzahl der Androiden
     */
    public void reparaturAuftragSenden(boolean sollSchutzschildRepariertWerden,
                                       boolean sollLebenserhaltungssystemRepariertWerden,
                                       boolean sollHuelleRepariertWerden, int anzahlAndroiden) {
        int anzahlDerZuReparierendenSchiffsstrukturen = 0;
        if (sollHuelleRepariertWerden)
            anzahlDerZuReparierendenSchiffsstrukturen++;
        if (sollLebenserhaltungssystemRepariertWerden)
            anzahlDerZuReparierendenSchiffsstrukturen++;
        if (sollSchutzschildRepariertWerden)
            anzahlDerZuReparierendenSchiffsstrukturen++;
        Random random = new Random();
        int randomNumberFromZeroToHundred = random.nextInt(101);
        if (anzahlAndroiden > reparaturAndroiden)
            anzahlAndroiden = reparaturAndroiden;
        double reparierteSchiffsstrukturen = randomNumberFromZeroToHundred / 100.0 * anzahlAndroiden / anzahlDerZuReparierendenSchiffsstrukturen;
        if (sollHuelleRepariertWerden)
            huelle += reparierteSchiffsstrukturen;
        if (sollLebenserhaltungssystemRepariertWerden)
            lebenserhaltungssysteme += reparierteSchiffsstrukturen;
        if (sollSchutzschildRepariertWerden)
            schutzschilde += reparierteSchiffsstrukturen;
    }

    /** Gibt das Raumschiff als Bezeichnung und Attributnamen mit seinen Zustaenden als String aus
     *
     * @return Ein String mit Bezeichnung, Attributnamen und deren Zustaenden
     */
    @Override
    public String toString() {
        return name + ":" + "\nEnergieversorgung: " + energieversorgung + "\nSchutzschilde: " + schutzschilde
                + "\nHülle: " + huelle + "\nLebenserhaltungssysteme: " + lebenserhaltungssysteme
                + "\nPhotonentorpedos: " + photonentorpedo + "\nAndroiden: " + reparaturAndroiden
                + "\nLadungsverzeichnis: \n" + ladungsVerzeichnisToString();
    }

    /** Gibt das Ladungsverzeichnis mit Ladungsnamen und Anzahl der Ladungen als String aus
     *
     * @return Ladungsnamen und Anzahl der jeweiligen Ladungen als ein String
     */
    private String ladungsVerzeichnisToString() {
        StringBuilder returnString = new StringBuilder();
        for (Ladung ladung : ladungsverzeichnis) {
            returnString.append(ladung.getName()).append(": ").append(ladung.getAnzahl()).append("\n");
        }
        return returnString.toString();
    }
}
