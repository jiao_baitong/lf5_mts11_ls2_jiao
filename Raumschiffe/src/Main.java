import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        List<Ladung> klingonenAnfangsladung = new ArrayList<>();
        Ladung ladung1 = new Ladung("Ferengi Schneckensaft", 200);
        Ladung ladung2 = new Ladung("Bat'leth Klingonen Schwert", 200);
        klingonenAnfangsladung.add(ladung1);
        klingonenAnfangsladung.add(ladung2);
        Raumschiff klingonen = new Raumschiff("IKZ Hegh'ta", 1, 1,
                1, 1, 1, 2, klingonenAnfangsladung);

        List<Ladung> romulanerAnfangsladung = new ArrayList<>();
        Ladung ladung3 = new Ladung("Borg-Schrott", 5);
        Ladung ladung4 = new Ladung("Rote Materie", 2);
        Ladung ladung5 = new Ladung("Plasma-Waffe", 50);
        romulanerAnfangsladung.add(ladung3);
        romulanerAnfangsladung.add(ladung4);
        romulanerAnfangsladung.add(ladung5);
        Raumschiff romulaner = new Raumschiff("IRW Khazara", 1, 1,
                1, 1, 2, 2, romulanerAnfangsladung);

        List<Ladung> vulkanierAnfangsladung = new ArrayList<>();
        Ladung ladung6 = new Ladung("Forschungssonde", 35);
        Ladung ladung7 = new Ladung("Photonentorpedo", 3);
        vulkanierAnfangsladung.add(ladung6);
        vulkanierAnfangsladung.add(ladung7);
        Raumschiff vulkanier = new Raumschiff("Ni'Var", 0.8, 0.8,
                1, 0.5, 0, 5, vulkanierAnfangsladung);

        System.out.println(klingonen);
        System.out.println(romulaner);
        System.out.println(vulkanier);

        klingonen.photonentorpedosAbschiessen(romulaner);
        romulaner.phaserkanonenAbschiessen(klingonen);
        Raumschiff.nachrichtAnAlleSenden(vulkanier.getName() + ": Gewalt ist nicht logisch");
        klingonen.zustandAufKonsoleAusgeben();
        klingonen.ladungsverzeichnisAufKonsoleAusgeben();
        vulkanier.reparaturAuftragSenden(true, true, true, 5);
        vulkanier.photonenTorpedosLaden(3);
        vulkanier.ladungsverzeichnisAufraeumen();
        klingonen.photonentorpedosAbschiessen(romulaner);
        klingonen.photonentorpedosAbschiessen(romulaner);
        klingonen.zustandAufKonsoleAusgeben();
        klingonen.ladungsverzeichnisAufKonsoleAusgeben();
        romulaner.zustandAufKonsoleAusgeben();
        romulaner.ladungsverzeichnisAufKonsoleAusgeben();
        vulkanier.zustandAufKonsoleAusgeben();
        vulkanier.ladungsverzeichnisAufKonsoleAusgeben();
        System.out.println(Raumschiff.getLogbuchEintraege());
    }

}
